# sass2css README
编译SASS文件到CSS

# 用法
在SASS文件的首行上加上一句JSON注释（不需要“{”和“}”）即可。
* 例子：
    ```scss
    // "out": "../css/style.css"
    
    body, html {
        ...
    }
    ```

# 设置项
`"main": { filepath: string }`
* 设置编译入口文件，当文件保存时候，将会编译main所指向的入口文件
* 当设置了main后，其余设置项均无效

`"out": { filepath: string }`
* 设置编译输出文件路径

`"style": { string }`
* 设置编译样式，支持nested, expanded, compact, compressed四种样式，缺省expanded

`"autoprefixer": { boolean | string[] }`
* 自动前缀browsers设置，例： "autoprefixer": ["last 2 versions", "> %5"]
* 当autoprefixer为true时候，采用["last 2 versions", "Firefox >= 20", "iOS 7", "> 5%"]设置