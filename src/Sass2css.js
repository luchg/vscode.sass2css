// vscode这个包，包含了里面所有的api
var vscode = require('vscode');
var Compiler = require("./Compiler");
var COMPILE_COMMAND = "Sass2css.compile";


function checkExt(document){
    return document && (document.fileName.endsWith(".sass") || document.fileName.endsWith(".scss"));
}

function activate(context) {
 
    var didCommands = vscode.commands.registerCommand(COMPILE_COMMAND, function () {
        var activeEditor = vscode.window.activeTextEditor;
        if (activeEditor) {
            var document = activeEditor.document;
            if (checkExt(document)) {
                var organise = new Compiler(document);
                organise.execute();
            }
            else {
                vscode.window.showWarningMessage("此命令只能编译.sass或者.scss文件。");
            }
        }
        else {
            vscode.window.showWarningMessage("此命令只能编译.sass或者.scss文件。");
        }
    });

    var didSaveEvent = vscode.workspace.onDidSaveTextDocument(function (document) {
        if (checkExt(document)) {
            var organise = new Compiler(document);
            organise.execute();
        }
    });
    
    context.subscriptions.push(didCommands);
    context.subscriptions.push(didSaveEvent);
}
exports.activate = activate;